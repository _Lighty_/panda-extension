(function ($) {
  var bg = chrome.extension.getBackgroundPage();
  String.prototype.format = function () {
    var content = this;
    for (var i = 0; i < arguments.length; i++) {
      var replacement = '{' + i + '}';
      content = content.replace(replacement, arguments[i]);
    }
    return content;
  };

  let menu = {
    CSS: {
      label: 'CSS mods',
      vars: {
        shoutboximagesize: {
          type: 'range',
          spacer: 1,
          text:
            'Shoutbox',
          vars: {
            label: 'Shoutbox image max size',
            host: 'forum',
            min: 0,
            max: 100
          }
        },
        shoutboxtimeonright: {
          type: 'boolean',
          vars: {
            label: 'Make the time ago stay on the right side',
            host: 'forum',
          }
        },
        gameme: {
          type: 'boolean',
          spacer: 1,
          text:
            '<b><span style="color: rgb(191, 191, 191)">game</span><span style="color: rgb(255, 128, 0)">ME</span></b>',
          vars: {
            label: 'gameME dark theme',
            host: 'gameme',
            callback: gameME_callback
          }
        },
        gameme_border: {
          type: 'enum',
          vars: {
            label: 'gameME borders',
            choices: { 'None': 'none', 'Dark': 'dark', 'Light': 'light' },
            callback: gameME_border_callback
          }
        },
        sourcebans: {
          type: 'boolean',
          spacer: 1,
          text:
            '<b><span style="color: rgb(255, 0, 0)">Source</span><span style="color: rgb(64, 64, 64)">Bans</span></b>',
          vars: {
            label: 'SourceBans dark theme',
            host: 'sourcebans',
          }
        },
        staffpage: {
          type: 'boolean',
          spacer: 1,
          text: '<b>Staff page</b>',
          vars: {
            label: 'Staff page dark theme',
            host: 'staffpage',
          }
        }
      }
    },
    JS: {
      label: 'JS mods',
      vars: {/*
      ignorethreads: {
        text: 'Forums',
        spacer: 1,
        type: 'boolean',
        vars: {
          label: 'Ignore threads',
          host: 'forum',
          isJS: true,
          callback: ignorethreads_callback
        }
      },
      ignorenodes: {
        type: 'boolean',
        vars: {
          label: 'Ignore subforums',
          host: 'forum',
          isJS: true,
          callback: ignorenodes_callback
        }
      },*/
        fixsidebarbeinglaggy: {
          type: 'boolean',
          vars: {
            label: 'Fix sidebar being laggy',
            tooltip: 'This also fixes the overall lagginess of the whole forums btw',
            host: 'forum',
            isJS: true,
          }
        },
        tabbedthreadview: {
          type: 'boolean',
          vars: {
            label: 'Make recent and new threads a single widget',
            host: 'forum',
            isJS: true,
          }
        },
        removeonlinestaff: {
          type: 'boolean',
          vars: {
            label: 'Remove Staff Online widget',
            host: 'forum',
            isJS: true,
          }
        },
        birthdayswidget: {
          type: 'boolean',
          vars: {
            label: 'Add a birthday widget to the sidebar!',
            host: 'forum',
            isJS: true,
          }
        },
        disablesnow: {
          type: 'boolean',
          vars: {
            label: 'Remove snow (winter only)',
            host: 'forum',
            isJS: true,
          }
        },
        extrashoutboxbuttons: {
          text: 'Shoutbox',
          spacer: 1,
          type: 'boolean',
          vars: {
            label: 'Extra buttons (img, url, bold, italic)',
            host: 'forum',
            isJS: true
          }
        },
        fixshoutbox: {
          type: 'boolean',
          vars: {
            label: 'Fix shoutbox (check tooltip (hover me!))',
            tooltip: 'Input doesn\'t get blocked after sending a message, doesn\'t stop showing messages after 10 minutes of not sending any messages, scrolling is 1000x better, auto makes URLs clickable as well as other peoples URLs, fixes reverse order or collapsed shoutbox being reset after browser restart',
            host: 'forum',
            isJS: true
          }
        },
        sblogolinktoforums: {
          text: 'SourceBans',
          spacer: 1,
          type: 'boolean',
          vars: {
            label: 'Make the logo link to forums',
            host: 'sourcebans',
            isJS: true
          }
        },
        resettopermanent: {
          type: 'boolean',
          vars: {
            label: 'Reset any unusual lengths to permanent',
            host: 'sourcebans',
            isJS: true
          }
        },
        sbaddcustompunishmenttime: {
          type: 'boolean',
          vars: {
            label: 'Add custom punishment length as an option',
            host: 'sourcebans',
            isJS: true
          }
        },
        sbfixlockup: {
          type: 'boolean',
          vars: {
            label: 'Fix page lockups (hover for more info)',
            tooltip: 'If you hover over any of the punishments while the page is loading, the page locks up and you won\'t be able to open any punishments',
            host: 'sourcebans',
            isJS: true
          }
        },
        sbfixunknownlength: {
          type: 'boolean',
          vars: {
            label: 'Fix ban reason being erased on strange ban lengths',
            host: 'sourcebans',
            isJS: true
          }
        },
        /*       sbfixplayernotfoundnameencoding: {
                type: 'boolean',
                vars: {
                  label: 'Fix unable to find player due to special chars',
                  host: 'sourcebans',
                  isJS: true
                }
              }, */
        sbpartiallydelayloading: {
          type: 'boolean',
          vars: {
            label: 'Speed up load times slightly (hover for more info)',
            tooltip: 'Tons of requests are made for server info in ADVANCED SEARCH, speedup may not be noticeable',
            host: 'sourcebans',
            isJS: true
          }
        },
        staffpage_autologin: {
          text: 'Staffpage',
          spacer: 1,
          type: 'boolean',
          vars: { label: 'Staffpage auto login' }
        },
        staffpage_group_alts: {
          type: 'boolean',
          vars: { label: 'Staffpage group alts in alt search' }
        },
        staffpagecloseredirecttab: {
          type: 'boolean',
          vars: {
            label: 'Close staffpage redirect tab (join link from callbot)',
            tooltip: 'WARNING! Make sure you make it auto open with steam, else it will break the link functionality!',
            callback: bg.setCloseRedirectTab
          }
        },
        staffpagecallbotmode: {
          type: 'boolean',
          vars: {
            label: 'Callbot mode (show notification and play a sound for calls)',
            tooltip: 'WARNING! Having multiple tabs of staffpage open at once is untested!'
          }
        },
        contextmenus: {
          text: 'Anywhere',
          spacer: 1,
          type: 'boolean',
          vars: {
            label: 'Context menus (search bans, comms, gameme or alts)',
            callback: bg.contexts
          }
        }
      }
    }
  };

  const styleMap = {
    'shoutboximagesize': 'div.bbWrapper>.lbContainer.lbContainer--inline{max-width:{0}%}',
    'forum_dark': bg.forum_css,
    'background': '#content{background-image: url("{0}"); margin-top: 0}',
    'stretch_background': '#content{background-size: 100%}',
    'transp_background':
      '#content .pageContent{background-color: #00000000 !important; border: 0 !important}',
    'banner': '#logoBlock .pageContent{background-image: url("{0}") !important}',
    'stretch_banner': '#logoBlock .pageContent{background-size: 100%}',
    'hideCovers': '.cover .CoverImage{display:none}.cover{height:0}',
    'hidetaigachat': '#taigachat_full{display:none}',
    'hideignored': '.taigachat_ignored{display:none!important}',
    'newicons': bg.newicons_css,
    'newemotes': bg.newemotes_css,
    'hideonline': '#widget-3{display:none}',
    'hidebirthdays': '#widget-8{display:none}',
    'hidestatistics': '#widget-4{display:none}',
    'none': 'td,th{border:0px solid #000}',
    'light': 'td,th{border:1px solid #585858}',
    'dark': ' ',
    'staffpage': bg.staffpage_css,
    'sourcebans': bg.sourcebans_css,
    'gameme': bg.gameme_css,
    'ignored': '.taigachat_ignored{display:none!important}',
    'covers': '.cover .CoverImage{display:none}.cover{height:0}',
    'shoutboxheight': '#taigachat_box{height:{0}px !important}',
    'shoutboxtimeonright': '.siropuShoutboxShouts>li time{float:right}',
  };

  const hostMap = {
    'forum': '*://www.panda-community.com/*',
    'gameme': '*://gameme.panda-community.com/*',
    'sourcebans': '*://bans.panda-community.com/*',
    'staffpage': '*://staff.panda-community.com/*',
  };

  function isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  }

  function customtaigajs_callback(val) {
    bg.setblockJS(val);
  }

  function replaceimages_callback() {
    var val = parseInt($(this).val());
    chrome.storage.local.get(['replaceimages'], items => {
      if (val != items.replaceimages) {
        save('replaceimages', val);
        Reload('forum');
      }
    });
  }

  function enablecustomemotes_callback(val) {
    var tab = $('#customemotes_tab');
    var orig = tab[0];
    if (!tab[0]) {
      $('<style/>')
        .append(
          'tbody {display:block;overflow:auto;}thead, tbody tr {display:table;width:100%}')
        .appendTo('head');
      tab = cTab('customemotes_tab', 'emotes');
      tab.css({ margin: '0px -20px', display: 'none' });
      chrome.storage.local.get(['enablecustomemotes', 'customemotes'], i => {
        if (!i.enablecustomemotes && !orig && !val) {
          $('#customemotes_tab_tab').remove();
          $('#customemotes_tab').remove();
          return;
        }
        let emotes = i.customemotes;
        if (!isEmpty(emotes)) {
          for (let name in emotes) {
            addToList(name, emotes[name]);
          }
        } else
          cText(
            tab,
            'Sorry sir/ma\'am, but there don\'t appear to be any <span style="text-decoration: line-through;">memes</span> I mean emotes');
        tab.append(
          '<div class="fixed-action-btn"><a href="#modal1" id="addEmote" class="btn-floating waves-effect waves-light modal-trigger"><i class="material-icons">add</i></a></div>');
        $('#addEmote').click(function () {
          $('#addemotemodal').openModal()
        });
        $('#emoteName, #emoteNameEdit').focusout(function () {
          let self = $(this);
          if (self.val().length) {
            if (self.val().length > 20) {
              self.removeClass('valid');
            } else {
              self.addClass('valid');
            }
          }
        });
        $('#emoteName, #emoteNameEdit').focusin(function () {
          $(this).removeClass('valid');
        });
        $('#emoteURL').on('input', function () {
          let self = $(this);
          if (self.val() && self.val().indexOf('http') == 0) {
            var img = new Image();
            img.onload = function () {
              $('#emoteurl-status').empty();
              if (this.width > 64 || this.height > 64) {
                self.addClass('invalid').removeClass('valid').removeClass(
                  'warning');
                $('#emotepreview').empty();
                $('#emoteurl-status').css('color', 'red');
              } else {
                self.addClass('valid').removeClass('invalid').removeClass(
                  'warning');
                $('#emotepreview').empty();
                let margin = (36 - this.height) / 2;
                $('#emoteurl-status').css('color', '#10ea11');
                $('<img>')
                  .attr('src', this.src)
                  .css('margin-top', margin + 'px')
                  .appendTo($('#emotepreview'));
                if (this.width > 32 || this.height > 32) {
                  self.addClass('warning');
                  $('#emoteurl-status').append('Image is of unusual size! ');
                  $('#emoteurl-status').css('color', '#ffef00');
                }
              }
              $('#emoteurl-status')
                .append(
                  'Width: ' + this.width + 'px/32px Height: ' + this.height +
                  'px/32px');
            };
            img.onerror = function () {
              self.addClass('invalid').removeClass('valid').removeClass(
                'warning');
              $('#emotepreview').empty();
              $('#emoteurl-status').empty();
              $('#emoteurl-status').append('Invalid image URL!');
              $('#emoteurl-status').css('color', 'red');
            };
            img.src = self.val();
          } else {
            self.removeClass('invalid').removeClass('valid').removeClass(
              'warning');
            $('#emotepreview').empty();
            $('#emoteurl-status').empty();
          }
        });
        $('#emoteURLEdit').on('input', function () {
          let self = $(this);
          if (self.val() && self.val().indexOf('http') == 0) {
            var img = new Image();
            img.onload = function () {
              $('#emoteurl-statusEdit').empty();
              if (this.width > 64 || this.height > 64) {
                self.addClass('invalid').removeClass('valid').removeClass(
                  'warning');
                $('#emotepreviewEdit').empty();
                $('#emoteurl-statusEdit').css('color', 'red');
              } else {
                self.addClass('valid').removeClass('invalid');
                $('#emotepreviewEdit').empty();
                let margin = (64 - this.height) / 2;
                $('#emoteurl-statusEdit').css('color', '#10ea11');
                $('<img>')
                  .attr('src', this.src)
                  .css('margin-top', margin + 'px')
                  .appendTo($('#emotepreviewEdit'));
                if (this.width > 32 || this.height > 32) {
                  self.addClass('warning');
                  $('#emoteurl-statusEdit').append('Image is of unusual size! ');
                  $('#emoteurl-statusEdit').css('color', '#ffef00');
                }
              }
              $('#emoteurl-statusEdit')
                .append(
                  'Width: ' + this.width + 'px/32px Height: ' + this.height +
                  'px/32px');
            };
            img.onerror = function () {
              self.addClass('invalid').removeClass('valid').removeClass(
                'warning');
              $('#emotepreviewEdit').empty();
              $('#emoteurl-statusEdit').empty();
              $('#emoteurl-statusEdit').append('Invalid image URL!');
              $('#emoteurl-statusEdit').css('color', 'red');
            };
            img.src = self.val();
          } else {
            self.removeClass('invalid').removeClass('valid').removeClass(
              'warning');
            $('#emotepreviewEdit').empty();
            $('#emoteurl-statusEdit').empty();
          }
        });
        $('#addemote').click(function () {
          let $emoteName = $('#emoteName');
          let $emoteURL = $('#emoteURL');
          if ($emoteName.hasClass('valid') && $emoteURL.hasClass('valid')) {
            if (!tab.find('#' + $emoteName.val().replace(/[^\w\s]/g, ''))[0]) {
              let url = $emoteURL.val().replace(/\?cEN=.*/, '');
              addToList($emoteName.val(), url);
              $('#addemotemodal').closeModal();
              emotes[$emoteName.val()] = url;
              chrome.storage.local.set({ customemotes: emotes });
              chrome.storage.sync.set({ customemotes: emotes });
              resetAddEmoteModal();
            } else {
              Materialize.toast(
                'Emote ' + $emoteName.val() + ' already exists!', 3000);
            }
          } else {
            let toast = '';
            if (!$emoteName.hasClass('valid')) {
              if (!$emoteURL.hasClass('valid')) {
                toast = 'No name and URL provided!';
              } else {
                if ($emoteName.val())
                  toast = 'Emote name is too long!';
                else
                  toast = 'No emote name provided!';
              }
            } else {
              toast = 'Invalid emote URL!';
            }
            Materialize.toast(toast, 3000);
          }
        });
        $('#saveemote').click(function () {
          let $emoteName = $('#emoteNameEdit');
          let $emoteURL = $('#emoteURLEdit');
          if ($emoteName.hasClass('valid') && $emoteURL.hasClass('valid')) {
            if (tab.find('#' + $emoteName.val().replace(/[^\w\s]/g, ''))[0] &&
              $emoteName.val() != $emoteName.attr('data-original')) {
              Materialize.toast(
                'Emote ' + $emoteName.val() + ' already exists!', 3000);
            } else {
              let url = $emoteURL.val().replace(/\?cEN=.*/, '');
              $('#editemotemodal').closeModal();
              delete emotes[$emoteName.attr('data-original')];
              emotes[$emoteName.val()] = url;
              chrome.storage.local.set({ customemotes: emotes });
              chrome.storage.sync.set({ customemotes: emotes });
              let emote = $(
                '#' + $emoteName.attr('data-original').replace(/[^\w\s]/g, ''));
              emote.attr('id', $emoteName.val().replace(/[^\w\s]/g, ''));
              emote.find('img').attr('src', url);
              emote.find('h6').text($emoteName.val());
            }
          } else {
            let toast = '';
            if (!$emoteName.hasClass('valid')) {
              if (!$emoteURL.hasClass('valid')) {
                toast = 'No name and URL provided!';
              } else {
                if ($emoteName.val())
                  toast = 'Emote name is too long!';
                else
                  toast = 'No emote name provided!';
              }
            } else {
              toast = 'Invalid emote URL!';
            }
            Materialize.toast(toast, 3000);
          }
        });
        if (bg.activetab == 'customemotes_tab_tab') tab.css({ display: 'block' });
      });
    } else {
      var isHidden = $('#customemotes_tab_tab').attr('style');
      if (val && isHidden)
        $('#customemotes_tab_tab').removeAttr('style')
      else if (!val && !isHidden)
        $('#customemotes_tab_tab').prop('style', 'display: none');
    }
    function addToList(name, url) {
      if (!tab.find('table')[0]) {
        if (tab.find('h6')[0]) tab.find('h6').remove();
        var table = cTable(tab, {}, 401);
        tab.find('table').prependTo(tab);
        addItem();
      } else
        addItem();
      function addItem() {
        let table = tab.find('tbody');
        var item = $(
          '<tr id="{0}" style="display: flex;transition: background-color .35s ease;align-items: center"/>'
            .format(name.replace(/[^\w\s]/g, '')));
        var img = new Image();
        img.src = url;
        $('<td style="width: 74px;padding: 0px 5px; display: flex;justify-content: center"/>')
          .append($('<img>').attr('src', url))
          .click(function () {
            editEmoteModal($(this));
          })
          .appendTo(item);
        $('<td/>')
          .append($('<h6>').append(name))
          .click(function () {
            editEmoteModal($(this));
          })
          .appendTo(item);
        $('<td class="right" style="margin-left: auto"/>')
          .append(
            $('<button class="btn waves-effect waves-teal" style="padding: 0px 15px">Remove</button>')
              .click(function () {
                $(this.parentNode.parentNode).slideUp({
                  duration: 350,
                  easing: 'easeOutQuart',
                  queue: !1,
                  complete: function () {
                    $(this).remove();
                  }
                });
                (function (name) {
                  chrome.storage.local.get(['customemotes'], i => {
                    delete i.customemotes[name];
                    chrome.storage.local.set(i);
                    chrome.storage.sync.set(i);
                  });
                })(name);
              }))
          .appendTo(item);
        table.append(item);
      }
    }
    function resetAddEmoteModal() {
      $('#emoteName').empty().removeClass('valid').val('');
      $('#emoteURL').empty().removeClass('valid').removeClass('warning').val('');
      ;
      $('#emotepreview').empty();
      $('#emoteurl-status').empty();
    }
    function editEmoteModal(emote) {
      parent = emote.parent();
      let name = parent.find('h6').text();
      let url = parent.find('img').attr('src');
      $('#emoteNameEdit')
        .val(name)
        .trigger('input')
        .trigger('focusout')
        .attr('data-original', name)
        .parent()
        .find('label')
        .addClass('active');
      $('#emoteURLEdit')
        .val(url)
        .removeClass('warning')
        .trigger('input')
        .parent()
        .find('label')
        .addClass('active');
      $('#editemotemodal').openModal();
    }
  }

  function ignorethreads_callback(val) {
    ignore__callback(
      val, ['threads', 'ignoredthreadids', 'threads', 'ignorethreads']);
  }

  function ignorenodes_callback(val) {
    ignore__callback(
      val, ['subforums', 'ignorednodeids', 'forums', 'ignorenodes']);
  }

  function ignore__callback(val, what) {
    var a = what[0];
    var b = what[1];
    var c = what[2];
    var d = what[3];

    var tab = $('#{0}_tab'.format(a));
    var orig = tab[0];
    if (!tab[0]) {
      $('<style/>')
        .append(
          'tbody {display:block;overflow:auto;}thead, tbody tr {display:table;width:100%}')
        .appendTo('head');
      tab = cTab('{0}_tab'.format(a), a);
      tab.css({ margin: '0px -20px', display: 'none' });
      chrome.storage.local.get([b, d], items => {
        if (!items[d] && !orig && !val) {
          $('#{0}_tab'.format(a)).remove();
          $('#{0}_tab_tab'.format(a)).remove();
          return;
        }
        var ignoreditems = items[b];
        if (!isEmpty(ignoreditems)) {
          var table = cTable(tab, {}, 401);
          for (var id in ignoreditems) {
            var item = $(
              '<tr id="{0}" style="display: flex;transition: background-color .35s ease"/>'
                .format(id));
            $('<td/>')
              .append(
                $('<div style="cursor: pointer"/>')
                  .append($('<h6/>').append(ignoreditems[id]))
                  .click(function () {
                    bg.openURL(
                      'https://www.panda-community.com/{1}/{0}/'.format(
                        this.parentNode.parentNode.getAttribute('id'),
                        c))
                  }))
              .appendTo(item)
            $('<td class="right" style="margin-left: auto"/>')
              .append(
                $('<button class="btn waves-effect waves-teal" style="padding: 0px 15px">Unignore</button>')
                  .click(function () {
                    $(this.parentNode.parentNode).slideUp({
                      duration: 350,
                      easing: 'easeOutQuart',
                      queue: !1,
                      complete: function () {
                        $(this).remove();
                      }
                    });
                    chrome.storage.local.get([b], details => {
                      delete details[b][this.parentNode.parentNode
                        .getAttribute('id')];
                      chrome.storage.local.set(details);
                      chrome.storage.sync.set(details);
                    });
                  }))
              .appendTo(item);
            table.append(item);
          }
        } else
          cText(tab, 'No ignored {0}.'.format(a));
        if (bg.activetab == '{0}_tab_tab'.format(a)) tab.css({ display: 'block' });
      });
    } else {
      var isHidden = $('#{0}_tab_tab'.format(a)).attr('style');
      if (val && isHidden)
        $('#{0}_tab_tab'.format(a)).removeAttr('style')
      else if (!val && !isHidden)
        $('#{0}_tab_tab'.format(a)).prop('style', 'display: none');
    }
  }

  function cTable(element, labels, size) {
    if (size > 356 && labels.length) size = 356;
    var table = $('<table class="striped"/>');
    var thead = $('<thead/>');
    var tbody = $('<tbody style="height: {0}px"/>'.format(size));
    var head = $('<tr/>');
    for (var label in labels)
      $('<th class="{0}" style="height: 45px"><h6 style="margin: -5px 0px">{1}</h6></th>'
        .format(labels[label], label))
        .appendTo(head);
    table.append(thead.append(head)).append(tbody).appendTo(element);
    return tbody;
  }

  function gameME_callback(val) {
    chrome.storage.local.get(['gameme_border'], items => {
      if (items.gameme_border != 'dark')
        chrome.tabs.query(
          { url: hostMap['gameme'] },
          (tabs) => tabs.forEach(t => {
            chrome.tabs.sendMessage(t.id, {
              reason: val ? 'add' : 'remove',
              theme: 'gameme_border',
              code: styleMap[items.gameme_border]
            })
          }))
    });
  }

  function gameME_border_callback() {
    var value = $(this).val();
    chrome.storage.local.get(['gameme_border', 'gameme'], items => {
      if (items.gameme_border != value) {
        save('gameme_border', value);
        if (!items.gameme)
          return;
        Send('gameme_border', false, 'gameme');
        Send('gameme_border', value, 'gameme', styleMap[value]);
      }
    });
  }

  function Reload(host) {
    chrome.tabs.query(
      { url: hostMap[host] },
      (tabs) => tabs.forEach(t => chrome.tabs.reload(t.id)));
  }

  function Send(theme, val, host, custom) {
    let request = {
      reason: val ? 'add' : 'remove',
      theme: theme,
    };
    if (val)
      if (custom)
        request['code'] = custom;
      else
        request['code'] = styleMap[theme].format(val);
    chrome.tabs.query(
      { url: hostMap[host] },
      (tabs) => tabs.forEach(t => chrome.tabs.sendMessage(t.id, request)));
  }

  function save(theme, val) {
    let save = {};
    save[theme] = val;
    chrome.storage.local.set(save);
    chrome.storage.sync.set(save);
  }
  var activeTab = chrome.extension.getBackgroundPage().activetab;

  for (let tab in menu) {
    let element = cTab(tab, menu[tab].label);
    let options = menu[tab].vars;
    for (let option in options) {
      let i = options[option];
      chrome.storage.local.get([option], items => {
        if (i.text) cText(element, i.text)
        if (i.spacer) cSpacer(element);
        switch (i.type) {
          case 'boolean':
            cCheckbox(element, option, items[option], i.vars);
            break;
          case 'string':
            cTextInput(element, option, items[option], i.vars);
            break;
          case 'enum':
            cRadio(element, option, items[option], i.vars);
            break;
          case 'range':
            cRange(element, option, items[option], i.vars)
        }
      });
    }
  }
  //ignorethreads_callback(false);
  //ignorenodes_callback(false);
  //enablecustomemotes_callback(false);
  $('.tab').click(function () {
    chrome.storage.local.set({ activetab: $(this).attr('id') });
    bg.activetab = $(this).attr('id');
  })

  function cCheckbox(element, id, checked, vars) {
    let checkVar = checked ? ' checked="checked"' : '';
    let tooltip = vars.tooltip ?
      ' class="tooltipped" data-position="bottom" data-tooltip="{0}"'.format(
        vars.tooltip) :
      '';
    element.append(
      '<div class=""><p><input id="{0}" type="checkbox" class="filled-in"{1} /><label for="{2}"{4}>{3}</label></p></div>'
        .format(id, checkVar, id, vars.label, tooltip));
    (function (id, target, vars) {
      target.click(() => {
        var val = $(target).is(':checked');
        if (vars.callback) vars.callback(val);
        save(id, val);
        if (vars.host)
          if (vars.isJS)
            Reload(vars.host);
          else
            Send(id, val, vars.host);
      })
    })(id, $('#' + id), vars);
  }

  function cTextInput(element, id, val, vars) {
    let valVar = '';
    let active = '';
    if (val) {
      valVar = ' value="{0}"'.format(val);
      active = ' class="active"';
    }
    element.append(
      ' <div class="input-field"><input id="{0}" type="text"{1} style="margin: 0px"/><label{4} for="{2}">{3}</label></div>'
        .format(id, valVar, id, vars.label, active));
    (function (id, target, vars) {
      target.keyup((event) => {
        if (event.keyCode === 13) {
          if (vars.callback) vars.callback(target.val());
          save(id, target.val());
          if (vars.host) {
            Send(id, false, vars.host);
            Send(id, target.val(), vars.host);
          }
        }
      });
    })(id, $('#' + id), vars);
  }

  function cRadio(element, id, val, vars) {
    var $div = $('<div/>');
    for (let i in vars.choices) {
      let choice = vars.choices[i]
      $div.append(
        $('<p/>')
          .append('<input id="{0}" type="radio" name="{1}" value="{2}"{3} />'
            .format(i, id, choice, val == choice ? 'checked' : ''))
          .css({ margin: '0px' })
          .append('<label for="{0}">{1}'.format(i, i)))
    }
    element.append($('<div/>')
      .addClass('col s12')
      .append($('<h6/>').append(vars.label))
      .append($div));
    if (vars.callback) {
      (function (id, callback) {
        $('input[name=' + id + ']').click(callback);
      })(id, vars.callback);
    }
  }

  function cRange(element, id, val, vars) {
    let tooltip = vars.tooltip ?
      ' class="tooltipped" data-position="bottom" data-tooltip="{0}"'.format(
        vars.tooltip) :
      '';
    element.append(
      '<div><h6 style="color:#9e9e9e">{0}</h6><p class="range-field" style="margin: 0"><input id="{1}" type="range"{2} value="{3}" min="{4}" max="{5}" /></p></div>'
        .format(vars.label, id, tooltip, val, vars.min, vars.max));
    (function (id, target, vars) {
      target.on('input', function () {
        var val = this.value;
        if (vars.callback) vars.callback(val);
        if (vars.host)
          if (vars.isJS)
            Reload(vars.host);
          else
            Send(id, parseInt(val), vars.host);
      });
      target.change(function () {
        var val = this.value;
        save(id, val);
      })
    })(id, $('#' + id), vars);
  }

  function cSpacer(element) {
    element.append($('<hr/>').css({ margin: '0px -7px' }));
  }

  function cText(element, text) {
    element.append('<h6>{0}</h6>'.format(text));
  }

  function cTab(id, label) {
    var isActive = activeTab == (id + '_tab');
    $('#tabs').append(
      '<li class="tab waves-effect waves-light" id="{2}"><a {3} href="#{0}">{1}</a></li>'
        .format(id, label, id + '_tab', isActive ? 'class="active"' : ''));
    $('#tabs_func').append('<div id="{0}" class=""></div>'.format(id));
    return $('#' + id);
  }
})(jQuery)