(function () {
  chrome.storage.local.get(
    {
      extrashoutboxbuttons: true,
      disablesnow: false,
      fixshoutbox: true,
      removeonlinestaff: false,
      version: '0',
      updateMSG: '',
      tabbedthreadview: true,
      shoutboximagesize: '0' /*
      ignorethreads: true,
      ignorenodes: true,
      ignoredthreadids: {},
      ignorednodeids: {}*/,
      fixsidebarbeinglaggy: true,
      shoutboxtimeonright: true,
      birthdayswidget: true
    },
    i => {
      document.addEventListener('DOMContentLoaded', () => {
        if ((i.ignorethreads || i.ignorenodes) && (document.URL == 'https://www.panda-community.com/' || document.URL.includes('https://www.panda-community.com/forums'))) {
          addCode('PTB-IgnoreButton', '.PTB-ignore-button{color:#959fb4;cursor:pointer}');
          // ignoreRecentNewThreadsOrNodes(i.ignorethreads, i.ignoredthreadids, i.ignorenodes, i.ignorednodeids);
        }
        if (document.URL == 'https://www.panda-community.com/' || document.URL == 'https://www.panda-community.com/shoutbox/fullpage') {
          addCode('fixsbprofpics', 'a.avatar{vertical-align: bottom}');
          addCode('fusbdev', '.siropuShoutbox .bbImage{ max-width: 100%');
          if (i.fixshoutbox) {
            let fixmemebox = document.createElement('script');
            fixmemebox.setAttribute('src', chrome.runtime.getURL('data/js/sbox.js'));
            document.documentElement.appendChild(document.importNode(fixmemebox, true));
          }
          if (parseInt(i.shoutboximagesize)) addCode('shoutboximagesize', 'div.bbWrapper>.lbContainer.lbContainer--inline{max-width:{0}%}'.format(i.shoutboximagesize));
          if (i.shoutboxtimeonright) addCode('shoutboxtimeonright', '.siropuShoutboxShouts>li time{float:right}');
        } else if (document.URL.includes('https://www.panda-community.com/account/account-details')) addCode('fixGenderBug', '.inputChoices label.iconic>input+i{position:relative;}');
        if (document.URL == 'https://www.panda-community.com/') {
          if (i.fixsidebarbeinglaggy) addJSblock('$(".uix_sidebar--scroller").trigger("sticky_kit:detach");');
          if (i.birthdayswidget) addBdayWidget();
        }
        let ver = chrome.runtime.getManifest().version;
        if (i.version !== ver && parseInt(i.version.replace(/\./g, '')) < parseInt(ver.replace(/\./g, ''))) {
          document.addEventListener('PTB-changelogSeen', () => {
            chrome.storage.local.set({ version: ver });
            chrome.storage.sync.set({ version: ver });
          });
          addJSblock(
            "XF.overlayMessage(\"Panda-ToolBox v{0} released!\", \"{1}\");$('.overlay-container.is-active').click(function(){var evt = document.createEvent('HTMLEvents');evt.initEvent('PTB-changelogSeen', true, true);document.dispatchEvent(evt);});".format(
              ver,
              i.updateMSG
            )
          );
        }
        if (i.disablesnow) addJSblock("if (typeof snowStorm !== 'undefined'){snowStorm.stop()}");
      });
      if (i.extrashoutboxbuttons && (document.URL == 'https://www.panda-community.com/' || document.URL == 'https://www.panda-community.com/shoutbox/fullpage')) {
        let shoutboxButtons = new MutationObserver(() => {
          if (document.querySelector('ol[class="siropuShoutboxShouts"]')) {
            addShoutboxButtons();
            shoutboxButtons.disconnect();
          }
        });
        shoutboxButtons.observe(document, { childList: true, subtree: true });
      }
      if ((i.removeonlinestaff || i.tabbedthreadview || i.birthdayswidget) && document.URL == 'https://www.panda-community.com/') {
        if (i.removeonlinestaff) {
          let removeonline = new MutationObserver(() => {
            let onlineAdmins = document.querySelector('div[data-widget-section="staffMembers"]');
            if (onlineAdmins) {
              onlineAdmins.remove();
              removeonline.disconnect();
            }
          });
          removeonline.observe(document, { childList: true, subtree: true });
        }
        if (i.tabbedthreadview) {
          let maketabbedthreadview = new MutationObserver(() => {
            let t1 = document.querySelector('div[data-widget-key="forum_overview_new_posts"]');
            let t2 = document.querySelector('div[data-widget-key="forum_overview_new_threads"]');
            let isDark = document.querySelector('link[href*="/styles/io_dark/"]');
            let isLight = document.querySelector('link[href*="/styles/io/"]');
            if (t1 && t2 && (isDark || isLight)) {
              createTabbedThreadView();
              maketabbedthreadview.disconnect();
            }
          });
          maketabbedthreadview.observe(document, { childList: true, subtree: true });
        }
        if (i.birthdayswidget) {
          let bdayWidgetAdder = new MutationObserver(() => {
            if (document.querySelector('div[class="uix_sidebar--scroller"]')) {
              addBdayPreloadWidget();
              bdayWidgetAdder.disconnect();
            }
          });
          bdayWidgetAdder.observe(document, { childList: true, subtree: true });
        }
      }
    }
  );
  function addBdayWidget() {
    function __bdayWidgetXF__() {
      function addBdayEntry(accLink, uID, imageHTML, styleData) {
        let li = $('<li>');
        let a = $('<a>');
        a.attr({
          href: accLink,
          class: 'avatar avatar--xs',
          'data-user-id': uID,
          'data-xf-init': 'member-tooltip',
          img: 'true',
          style: styleData
        });
        a.html(imageHTML);
        return li.append(a);
      }
      XF.ajax('GET', XF.canonicalizeUrl('members/?key=todays_birthdays'), {}, function(h) {
        if (h.html) {
          let el = $('<div>');
          el.html(h.html.content);
          el = el.find('ol[class="block-body"]');
          let child = el.children().first();
          let listEl = $('#bdaysList');
          listEl.html('');
          if (!(child.find('div[class="contentRow-figure"]')[0])) {
            listEl[0].outerHTML = '<span class="">How odd, no one has a birthday today!</span>';
            return;
          }
          while (child[0]) {
            let temp = child
              .find('div[class="contentRow-figure"]')
              .children()
              .first();
            let accLink = temp.attr('href');
            let uID = temp.data('user-id');
            let styleData = temp.attr('style');
            temp = temp.children().first();
            let imageHTML = temp.prop('outerHTML');
            let bdayEntry = addBdayEntry(accLink, uID, imageHTML, styleData);
            XF.activate(bdayEntry); // tooltip
            listEl.append(bdayEntry);
            child = child.next();
          }
        }
      }, {
        global: false,
      });
    }
    addJSblock(__bdayWidgetXF__.toString() + '__bdayWidgetXF__();');
  }
  function addBdayPreloadWidget() {
    let isDarkTheme = document.querySelector('link[href*="/styles/io_dark/"]');
    function insertAfter(el, referenceNode) {
      referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    }
    function preLoadProfilePic() {
      let li = document.createElement('li');
      let a = document.createElement('a');
      setAttributes(a, {
        href: '/members/0/',
        class: 'avatar avatar--xs avatar--default avatar--default--dynamic',
        'data-user-id': '0',
        img: 'true',
        style: 'background-color: ' + (isDarkTheme ? '#4c535f' : '#e1e1e1')
      });
      li.appendChild(a);
      return li;
    }
    addCode('PTB-bdays', '.block[data-widget-section="bdays"] .block-minorHeader:before{content: \'\\f0eb\'');
    let root = document.createElement('div');
    setAttributes(root, {
      class: 'block',
      'data-widget-section': 'bdays'
    });
    let container = document.createElement('div');
    container.setAttribute('class', 'block-container');
    let minorHeader = document.createElement('h3');
    minorHeader.setAttribute('class', 'block-minorHeader');
    let header = document.createElement('a');
    header.innerText = 'Todays birthdays';
    header.setAttribute('href', '/members/?key=todays_birthdays');
    let body = document.createElement('div');
    body.setAttribute('class', 'block-body');
    let blockRow = document.createElement('div');
    setAttributes(blockRow, {
      class: 'block-row block-row--minor',
      style: 'padding-left: 8px;'
    });
    let list = document.createElement('ul');
    setAttributes(list, {
      class: 'listHeap',
      id: 'bdaysList'
    });
    for (let i = 0; i < 6; i++) list.appendChild(preLoadProfilePic());
    blockRow.appendChild(list);
    body.appendChild(blockRow);
    minorHeader.appendChild(header);
    container.appendChild(minorHeader);
    container.appendChild(body);
    root.appendChild(container);
    insertAfter(root, document.querySelector('div[data-widget-section="onlineNow"]'));
  }

  function ignoreButton() {
    let el = document.createElement('i');
    setAttributes(el, { title: 'Ignore', class: 'fa fa-ban PTB-ignore-button', 'aria-hidden': 'true' });
    return el;
  }
  function setAttributes(el, attrs) {
    for (var key in attrs) {
      el.setAttribute(key, attrs[key]);
    }
  }
  function saveIgnoredThread(id, title) {
    chrome.storage.local.get({ ignoredthreadids: {} }, details => {
      details.ignoredthreadids[id] = title;
      chrome.storage.local.set(details);
      chrome.storage.sync.set(details);
    });
  }
  function saveIgnoredNode(id, title) {
    chrome.storage.local.get({ ignorednodeids: {} }, details => {
      details.ignorednodeids[id] = title;
      chrome.storage.local.set(details);
      chrome.storage.sync.set(details);
    });
  }
  function ignoreRecentNewThreadsOrNodes(threads, ignoredthreads, nodes, ignorednodes) {
    let threadLists = [];
    let newpostsbase = document.querySelector('div[data-widget-key="forum_overview_new_posts"]');
    threadLists[0] = newpostsbase.querySelector('ul.block-body');
    threadLists[1] = newpostsbase.nextElementSibling.querySelector('ul.block-body');
    for (let i = 0; i < 2; i++) {
      let threads = threadLists[i].getElementsByClassName('block-row');
      for (let ii = 0; ii < threads.length; ii++) {
        let el = threads[ii];
        let threadid = el
          .querySelector('div.contentRow-main>a')
          .getAttribute('href')
          .match(/(?:[^\/]+\.([\d]+)\/)/)[1];
        let nodeid = el
          .querySelector('div.contentRow-minor.contentRow-minor--hideLinks>a')
          .getAttribute('href')
          .match(/(?:[^\/]+\.([\d]+)\/)/)[1];
        let threadtitle = el.querySelector('div.contentRow-main>a').innerText;
        let nodetitle = el.querySelector('div.contentRow-minor.contentRow-minor--hideLinks>a').innerText;
        if ((threads && ignoredthreads.hasOwnProperty(threadid)) || (nodes && ignorednodes.hasOwnProperty(nodeid))) {
          let old = threads.length;
          el.remove();
          ii--;
          if (old === threads.length) {
            console.log('failure');
            break;
          }
          continue;
        }
        if (nodes) {
          let ib = ignoreButton();
          (function (id, title) {
            ib.addEventListener('click', function () {
              saveIgnoredNode(id, title);
              let temp = this;
              for (let t = 0; t < 4; t++) temp = temp.parentElement;
              temp.remove();
            });
          })(nodeid, nodetitle);
          el.querySelectorAll('.contentRow-minor.contentRow-minor--hideLinks')[1].appendChild(ib);
        }
      }
      if (!threads.length) threadLists[i].appendChild(emptyListWarning());
    }
    function emptyListWarning() {
      let li = document.createElement('li');
      li.classList.add('block-row');
      let div = document.createElement('div');
      div.classList.add('contentRow');
      let i = document.createElement('i');
      setAttributes(i, {
        class: 'fa fa-exclamation-circle',
        'aria-hidden': 'true',
        style: 'padding-top: 1px; padding-right: 3px'
      });
      let div2 = document.createElement('div');
      div2.innerText = 'All threads ignored';
      setAttributes(div2, {
        style: 'font-size: 10px'
      });
      div.appendChild(i);
      div.appendChild(div2);
      li.appendChild(div);
      return li;
    }
  }

  function addJSblock(code) {
    let el = document.createElement('script');
    el.setAttribute('type', 'text/javascript');
    el.appendChild(document.createTextNode(code));
    document.documentElement.appendChild(document.importNode(el, true));
  }

  function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
  }

  function getCaretPosition(ctrl) {
    var CaretPos = 0;
    if (document.selection) {
      ctrl.focus();
      var Sel = document.selection.createRange();
      Sel.moveStart('character', -ctrl.value.length);
      CaretPos = Sel.text.length;
    } else if (ctrl.selectionStart || ctrl.selectionStart == '0') CaretPos = ctrl.selectionStart;
    return CaretPos;
  }
  function getCaretLength(ctrl) {
    var CaretPos = 0;
    if (document.selection) {
      ctrl.focus();
      var Sel = document.selection.createRange();
      CaretPos = Sel.text.length;
    } else if (ctrl.selectionEnd || ctrl.selectionEnd == '0') CaretPos = ctrl.selectionEnd - ctrl.selectionStart;
    return CaretPos;
  }
  function setCaretPosition(ctrl, pos) {
    if (ctrl.setSelectionRange) {
      ctrl.focus();
      ctrl.setSelectionRange(pos, pos);
    } else if (ctrl.createTextRange) {
      var range = ctrl.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  }
  function insertAroundCaret(el, myValue, myValue2) {
    if (document.selection) {
      el.focus();
      sel = document.selection.createRange();
      sel.text = myValue + sel.text + myValue2;
      el.focus();
    } else if (el.selectionStart || el.selectionStart == '0') {
      var startPos = el.selectionStart;
      var endPos = el.selectionEnd;
      var scrollTop = el.scrollTop;
      el.value = el.value.substring(0, startPos) + myValue + el.value.substring(startPos, endPos) + myValue2 + el.value.substring(endPos, el.value.length);
      el.focus();
      el.selectionStart = startPos + myValue.length + myValue2.length + (endPos - startPos);
      el.selectionEnd = startPos + myValue.length + myValue2.length + (endPos - startPos);
      el.scrollTop = scrollTop;
    } else {
      el.value += myValue + myValue2;
      el.focus();
    }
  }
  function addShoutboxButtons() {
    var toolbar = document.querySelector('form[data-xf-init="siropu-shoutbox-submit"]').firstElementChild;
    var options = {
      image: { 'fa-image': '[img][/img]' },
      url: {
        'fa-link': '[url=][/url]'
      },
      bold: { 'fa-bold': '[B][/B]' },
      italic: {
        'fa-italic': '[I][/I]'
      }
    };
    let reference = toolbar.firstElementChild.nextElementSibling;
    for (let type in options) {
      let option = options[type];
      for (let icon in option) {
        let el = htmlToElement('<button type="button" class="button button--link rippleButton"><i class="fal {0}" aria-hidden="true"></i><div class="ripple-container"></div></button>'.format(icon));
        el.addEventListener('click', function (e) {
          var bbcode = option[icon];
          e.stopPropagation();
          var position = bbcode.length;
          var oldel = document.querySelector('input[name="shout"]');
          var ins = getCaretLength(oldel) > 0;
          insertAroundCaret(oldel, bbcode.substring(0, bbcode.indexOf('][') + 1), bbcode.substring(bbcode.indexOf('][') + 1, bbcode.length));
          if (bbcode.indexOf('=][') != -1) {
            position = bbcode.indexOf('=][') + 1;
          } else {
            position = bbcode.indexOf('][') + 1;
          }
          if (!ins) setCaretPosition(oldel, getCaretPosition(oldel) - (bbcode.length - position));
          else setCaretPosition(oldel, getCaretPosition(oldel) + bbcode.length - position);
        });
        toolbar.insertBefore(el, reference);
      }
    }
  }
  function createTabbedThreadView() {
    let isDarkTheme = document.querySelector('link[href*="/styles/io_dark/"]');
    addCode(
      'tabbedThreadView',
      '.tab-container{display: grid}#recentthreads.block-body,#newthreads.block-body{grid-column:1;grid-row:1;border-radius:8px;border-top-left-radius:0;border-top-right-radius:0}.tabs{-webkit-user-select:none;display: flex;margin: 0;padding: 0;text-align: center}.tabs>.tab:first-child{border-top-left-radius:8px}.tabs>.tab:last-child{border-top-right-radius:8px}.tab{background:{2};flex: 1 0 auto;list-style: none;padding: 10px;transition: all .2s ease;cursor: pointer}.tab:hover{color: {0}}.inactive-tab{background-color: {1}}.inactive-tab:hover{background-color:{2}}'.format(
        isDarkTheme ? 'white' : 'black',
        isDarkTheme ? '#282d35' : '#e1e1e1',
        isDarkTheme ? '#313742' : '#fff'
      )
    );
    let base = document.querySelector('div[data-widget-key="forum_overview_new_posts"]').firstElementChild;
    base.querySelector('h3.block-minorHeader').remove();
    base.firstElementChild.setAttribute('id', 'recentthreads');
    let secondbase = document.querySelector('div[data-widget-key="forum_overview_new_threads"]');
    let newthreads = secondbase.querySelector('ul.block-body');
    newthreads.setAttribute('style', 'display: none');
    newthreads.setAttribute('id', 'newthreads');
    let tabContentsBase = document.createElement('div');
    tabContentsBase.classList.add('tab-container');
    tabContentsBase.appendChild(base.firstElementChild);
    tabContentsBase.appendChild(newthreads);
    base.appendChild(tabContentsBase);
    secondbase.remove();
    let tabsBase = document.createElement('ul');
    tabsBase.setAttribute('class', 'tabs');
    tabsBase.appendChild(createTab('Recent threads', 'recentthreadstab', true));
    tabsBase.appendChild(createTab('New threads', 'newthreadstab', false));
    tabsBase.querySelector('#recentthreadstab').addEventListener('click', function () {
      if (!this.classList.contains('inactive-tab')) return;
      this.classList.remove('inactive-tab');
      this.nextElementSibling.classList.add('inactive-tab');
    });
    tabsBase.querySelector('#newthreadstab').addEventListener('click', function () {
      if (!this.classList.contains('inactive-tab')) return;
      this.classList.remove('inactive-tab');
      this.previousElementSibling.classList.add('inactive-tab');
    });
    base.prepend(tabsBase);
    document.addEventListener('DOMContentLoaded', function(){
      addJSblock("$('#newthreadstab').click(function(){$('#newthreads').fadeIn(260);$('#recentthreads').fadeOut(260);});$('#recentthreadstab').click(function(){$('#newthreads').fadeOut(260);$('#recentthreads').fadeIn(260);});");
    });
    function createTab(name, id, active) {
      let tab = document.createElement('li');
      tab.setAttribute('class', 'tab u-ripple' + (active ? '' : ' inactive-tab'));
      tab.setAttribute('id', id);
      tab.innerText = name;
      return tab;
    }
  }
})();
