(function() {
var sHandler = XF.Element.getHandler(
    $('[data-xf-init~=siropu-shoutbox]'), 'siropu-shoutbox');
sHandler.update = function(h, g) {
  var f = this;
  var e = this.$target.find('.siropuShoutboxShouts');
  if (h.html && h.html.content) {
    XF.setupHtmlInsert(h.html, function(i, l, m) {
      var j = $.grep(i, function(n) {
          return e.find('li[data-id="' + $(n).data("id") + '"]').length ? false : true
      });
      var k = e.find('> li:not([data-id])');
      if (k.length) {
        k.remove()
      }
      var n = f.options.lastScroll < new Date().getTime() - 20000 ||
          e.get(0).scrollHeight - e.get(0).scrollTop <= 407;
      fixNonLinks($(j).find('.bbWrapper'));
      if (f.options.reverse) {
        $(j).show();
        $(j).appendTo(e);
        if (n) {
          setTimeout(function() {
            e.scrollTop(100000)
          }, 500);
          e.scrollTop(100000)
        }
      } else {
        $(j).hide();
        $(j).prependTo(e);
        $(j).slideDown();
        if (n) {
          setTimeout(function() {
            e.scrollTop(0)
          }, 500);
          e.scrollTop(0)
        }
      }
      if (n) f.options.lastScroll = 0;
      if (g === undefined) {
        f.playSound()
      }
    })
  }
  if (h.lastId) {
    f.setLastId(h.lastId)
  }
};
var g = false;
$('.siropuShoutboxShouts').off('scroll');
$('.siropuShoutboxShouts').on('scroll', function(j) {
  var e = $('.siropuShoutboxShouts');
  let f = sHandler;
  f.options.lastScroll = new Date().getTime();
  var l = $(this)[0].scrollHeight - $(this).innerHeight();
  var j = parseInt($(this).scrollTop());
  if (f.options.loadMore && !g &&
      (!f.options.reverse && (j == l || j + 1 == l || j - 1 == l) ||
       f.options.reverse && j == 0)) {
    g = true;
    var k = $(this)
                .find('> li:' + (f.options.reverse ? 'first' : 'last'))
                .data('id');
    var i = $('<div class="siropuShoutboxLoadingMoreShouts" />');
    i.html(
        '<i class="fa fa-cog fa-spin"></i> ' +
        XF.phrase('siropu_shoutbox_loading_more_shouts'));
    if (f.options.reverse) {
      i.addClass('siropuShoutboxReverse').insertBefore($(this))
    } else {
      i.insertAfter($(this))
    }
    var h = $(this).parents().find('.siropuShoutboxLoadingMoreShouts');
    setTimeout(function() {
      h.fadeOut();
      h.remove()
    }, 600);
    XF.ajax(
        'POST', XF.canonicalizeUrl('index.php?shoutbox/load-more'),
        {last_id: k}, function(m) {
          if (m.html && m.html.content) {
            XF.setupHtmlInsert(m.html, function(n, o, p) {
              fixNonLinks($(n).find('.bbWrapper'));
              if (f.options.reverse) {
                var referencechild = e.children().first();
                e.prepend(n);
                e.scrollTop(
                    referencechild.get()[0].getBoundingClientRect().top -
                    e.children().first().get()[0].getBoundingClientRect().top)
              } else {
                e.append(n)
              }
            })
          } else {
            f.options.loadMore = 0
          }
          g = false
        })
  }
});
$(document).off('shoutbox:submit');
$(document).on('shoutbox:submit', function(j) {
  var h = $(this).find('input[name="shout"]');
  var i = h.val().trim().replace(
      /(?<!["'\]=])(https?:\/\/([-\w\.]+[-\w])+(:\d+)?(\/([\w/_\.#-%-]*(\?\S+)?[^\.\s])?)?)/g,
      '[url=$1]$1[/url]');
  if (!i) {
    return h.focus();
  }
  var k = new Date().getTime() - sHandler.options.lastActive;
  if (k < 4100) {
    XF.flashMessage('Hold on there pal, too quick!', 2000);
    h.prop('disabled', true);
    setTimeout(function() {
      $(document).trigger('shoutbox:submit');
    }, 4100 - k);
    return;
  }
  h.val('').prop('disabled', false);
  sHandler.stopRefreshInterval();
  XF.ajax(
      'POST', XF.canonicalizeUrl('index.php?shoutbox/submit'),
      {last_id: sHandler.options.lastId, shout: i}, function(l) {
        sHandler.update(l);
        var e = $('.siropuShoutboxShouts');
        if (sHandler.options.reverse) {
          setTimeout(function() {
            e.scrollTop(100000)
          }, 500);
          e.scrollTop(100000)
        } else {
          setTimeout(function() {
            e.scrollTop(0)
          }, 500);
          e.scrollTop(0)
        }
        sHandler.startRefreshInterval();
        sHandler.options.lastActive = new Date().getTime();
        sHandler.options.lastScroll = 0;
      }, {global: false});
});
var oldCookieSet = XF.Cookie.set;
XF.Cookie.set = function(a, b, c) {
  if (a.includes('siropuShoutbox'))
    c = new Date((new Date() / 1000 + 86400 * 30 * 12) * 1000);
  oldCookieSet(a, b, c);
}; // Keeping it just in case knew it was gonna be needed
$('.siropuShoutboxShouts').children().each(function(){fixNonLinks($(this).find('.bbWrapper'))});
function fixNonLinks(root) {
  $(root).contents().each(function(){
    if (this.nodeType == Node.TEXT_NODE)
      $(this).replaceWith(this.textContent.replace(
        /(?<!["'\]=])(https?:\/\/([-\w\.]+[-\w])+(:\d+)?(\/([\w/_\.#-%-]*(\?\S+)?[^\.\s])?)?)/g,
        '<a href="$1" target="_blank" class="link link--external" rel="noopener">$1</a>'));
    else if (this.tagName == 'A')
        return;
    fixNonLinks(this);
  });
}
})();