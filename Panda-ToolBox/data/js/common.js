var g_styleElements = {};

chrome.runtime.onMessage.addListener(function(request) {
  switch (request.reason) {
    case 'remove':
      removeStyle(request.theme);
      break;
    case 'add':
      addCode(request.theme, request.code);
  }
});

function removeStyle(id) {
  var e = document.getElementById('PTB-' + id);
  delete g_styleElements['PTB-' + id];
  if (e) {
    e.remove();
  }
}

function addCode(theme, style) {
  var styleElement = document.getElementById('PTB-' + theme);
  // Already there.
  if (styleElement) styleElement.remove();
  styleElement = document.createElement('style');
  styleElement.setAttribute('id', 'PTB-' + theme);
  styleElement.setAttribute('type', 'text/css');
  styleElement.appendChild(document.createTextNode(style));
  document.documentElement.appendChild(document.importNode(styleElement, true));
  g_styleElements[styleElement.id] = styleElement;
}

String.prototype.format = function() {
  var content = this;
  for (var i = 0; i < arguments.length; i++) {
    var replacement = '{' + i + '}';
    content = content.replace(new RegExp(escapeRegExp(replacement), 'g'), arguments[i]);
  }
  return content;
  function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }
};

String.prototype.formatSingle = function() {
  var content = this;
  for (var i = 0; i < arguments.length; i++) {
    var replacement = '{' + i + '}';
    content = content.replace(replacement, arguments[i]);
  }
  return content;
};