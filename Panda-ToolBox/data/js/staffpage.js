(function () {
  function toCommId(steamid) {
    var split = steamid.split(':');
    return (BigInt('76561197960265728') + BigInt(split[2] * 2) + BigInt(split[1])).toString();
  }
  chrome.storage.local.get(
    {
      staffpage: true,
      staffpage_autologin: true,
      staffpage_group_alts: true,
      staffpage_css: '',
      altsearch: '',
      staffpage_just_logged_in: false,
      staffpagelastcallserverscheck: 0,
      staffpagecallservers: [],
      staffpagegrabcallservers: false,
      staffpagecalls: [],
      staffpagecallbotmode: false,
    },
    i => {
      if (i.staffpage) addCode('staffpage', i.staffpage_css);
      if (document.location.href == 'https://staff.panda-community.com/login') {
        chrome.storage.local.set({ staffpage_just_logged_in: true });
        document.location.href =
          'https://steamcommunity.com/openid/login?openid.ns=http://specs.openid.net/auth/2.0&openid.mode=checkid_setup&openid.return_to=https://staff.panda-community.com/login&openid.realm=https://staff.panda-community.com&openid.identity=http://specs.openid.net/auth/2.0/identifier_select&openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select';
        return;
      }
      if (i.staffpage_just_logged_in) {
        chrome.storage.local.set({ staffpage_just_logged_in: false });
        if (i.altsearch) {
          window.location.href = '/altlookup';
          return;
        }
      }
      let searchalt = i.altsearch && document.URL.includes('altlookup');
      let groupalts = i.staffpage_group_alts && document.URL.includes('altlookup');
      if (searchalt || groupalts) {
        document.addEventListener('DOMContentLoaded', () => {
          if (searchalt) searchAlt(i.altsearch);
          else if (groupalts) groupAlts();
        });
        return;
      }
      if (i.staffpagecallbotmode && document.URL === 'https://staff.panda-community.com/')
        document.addEventListener('DOMContentLoaded', () => callbotMode(i.staffpagelastcallserverscheck, i.staffpagecallservers, i.staffpagecalls));
      if (i.staffpagegrabcallservers && document.URL === 'https://staff.panda-community.com/settings') {
        document.addEventListener('DOMContentLoaded', () => {
          let servers = [];
          document.querySelector('.row:nth-of-type(3) [tabindex=\'0\']').querySelectorAll('.ui.label').forEach(function (el) {
            servers.push(el.firstChild.textContent.trim());
          });
          console.log(`Got call servers ${servers}`);
          chrome.storage.local.set({ staffpagecallservers: servers, staffpagegrabcallservers: false, staffpagelastcallserverscheck: (new Date().getTime() / 1000) }, function () {
            location.href = 'https://staff.panda-community.com/';
          });
        });
      }
    }
  );

  function callbotMode(lastcheck, servers, calls) {
    let notifSound = new Audio(chrome.runtime.getURL('data/light.mp3'));
    if (!Notification) {
      console.error('Notifications unsupported!');
      return;
    }
    if (Notification.permission === 'denied') {
      console.error('No notification permissions!');
      return;
    }

    // 3 days
    if ((new Date().getTime() / 1000) - lastcheck > 259200 || !servers || !servers.length) {
      chrome.storage.local.set({ staffpagegrabcallservers: true }, function () {
        location.href = 'https://staff.panda-community.com/settings';
      });
      return;
    }

    let activeNotifications = [];
    let reloadOnClose = false;
    let reloadInterval;
    let reactivateTimeout;
    let doCallbotTimeout;

    function makeNotification(server, data, id, joinurl) {
      let notification = new Notification(`New call from ${server}`, {
        vibrate: true,
        body: data,
        data: {
          id: id,
          url: joinurl,
        }
      });
      notification.onclick = notificationClicked;
      notification.onclose = notificationClosed;
      activeNotifications.push(id);
      notifSound.play();
    }
    function notificationClicked() {
      console.log('Notification clicked');
      this.close();
      location.href = this.data.url;
    }
    function notificationClosed() {
      console.log('Notification closed');
      for (var i = 0; i < activeNotifications.length; i++) {
        if (activeNotifications[i] === this.data.id) {
          activeNotifications.splice(i, 1);
        }
      }
      if (!activeNotifications.length && reloadOnClose) {
        console.log('No active notifications and requested to reload, reloading');
        location.reload();
      }
    }
    reloadInterval = setInterval(() => {
      if (activeNotifications.length) {
        console.log('Active notification, waiting.');
        reloadOnClose = true;
        return;
      }
      location.reload();
    }, 60 * 1000);
    doCallbotTimeout = setTimeout(() => {
      console.log('Show calls');
      let changed = false;
      document.querySelector('.accordion').querySelectorAll('.accordion .title').forEach(function (call) {
        if (call.querySelector('span[title]').innerText.includes(' hour'))
          return;
        var child = call.firstChild;
        let server = '';
        while (child = child.nextSibling) {
          if (child.nodeType == Node.TEXT_NODE) {
            server = child.textContent.trim();
            server = server.substr(0, server.indexOf(':')).trim();
            break;
          }
        }

        let callContent = call.nextElementSibling;

        let joinurl = callContent.querySelector('a[href]').href;

        let data = '';

        var child = callContent.firstChild;
        let reporter = '';
        let target = '';
        let reason = '';
        let callid = '';
        while (child = child.nextSibling) {
          if (child.nodeType == Node.TEXT_NODE) {
            let str = child.textContent.trim();
            if (!str.length)
              continue;
            if (str.startsWith('Call/Handle ID: '))
              callid = str.substr(str.indexOf('#') + 1).trim();
            else if (str.startsWith('Reporter: '))
              reporter = str.substr(0, str.indexOf('(STEAM_')).trim();
            else if (str.startsWith('Target: '))
              target = str.substr(0, str.indexOf('(STEAM_')).trim();
            else if (str.startsWith('Reason: ')) {
              reason = str;
              break;
            }
          }
        }
        data = `${reporter}\n${target}\n${reason}`;
        if (calls.includes(callid))
          return;

        if (!servers.includes(server))
          return;
        console.log('Got call from our server!');
        calls.push(callid);
        changed = true;
        makeNotification(server, data, callid, joinurl);
      })
      while (calls.length > 25) {
        changed = true;
        calls.shift();
      }
      if (changed)
        chrome.storage.local.set({ staffpagecalls: calls });
    }, 3000);
    document.addEventListener('scroll', function () {
      if (reactivateTimeout) {
        clearTimeout(reactivateTimeout);
        reactivateTimeout = setTimeout(() => { window.scrollTo(0, 0); location.reload() }, 30 * 1000);
      }
      if (reloadInterval) {
        console.log('User scrolled, cancelling auto reload');
        clearInterval(reloadInterval);
        clearTimeout(doCallbotTimeout);
        doCallbotTimeout = reloadInterval = 0;
        reactivateTimeout = setTimeout(() => { window.scrollTo(0, 0); location.reload() }, 30 * 1000);
      }
    });
  }
  function groupAlts() {
    let table = document.querySelector('tbody');
    if (!table) return;
    let child = table.firstElementChild;
    let alts = {};
    while (child) {
      let info = child.querySelectorAll('td');
      let name = info[0].innerText;
      let steamID = info[1].innerText;
      if (!alts[steamID]) alts[steamID] = [];
      alts[steamID].push(name);
      child = child.nextElementSibling;
    }
    table.parentNode.replaceChild(table.cloneNode(false), table);
    table = document.querySelector('tbody');
    for (let steamID in alts) {
      let names = '<td>';
      alts[steamID].forEach(name => {
        if (!names.includes(name)) names += (names.length == 4 ? '' : '<br>') + name;
      });
      names += '</td>';
      let therest = '<td>{0}</td><td><a target="_blank" href="https://gameme.panda-community.com/search?si=players&rc=all&q={0}">gameME</a> | <a target="_blank" href="https://bans.panda-community.com/index.php?p=banlist&searchText={0}">Ban list</a> | <a target="_blank" href="https://bans.panda-community.com/index.php?p=commslist&searchText={0}">Comms list</a> | <a target="_blank" href="http://steamcommunity.com/profiles/{1}">Profile link</a></td>'.format(
        steamID,
        toCommId(steamID)
      );
      table.insertAdjacentHTML('beforeend', names + therest);
    }
  }
  function searchAlt(id) {
    document.querySelector('input[name=search]').value = id;
    document.querySelector('button[type=submit]').click();
    chrome.storage.local.set({ altsearch: '' });
  }
})();
