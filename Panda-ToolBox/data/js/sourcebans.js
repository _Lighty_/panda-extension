(function() {
  function setAttributes(el, attrs) {
    for (var key in attrs) {
      el.setAttribute(key, attrs[key]);
    }
  }
  chrome.storage.local.get(
    {
      sourcebans: true,
      sourcebans_css: '',
      sblogolinktoforums: false,
      resettopermanent: true,
      sbfixlockup: true,
      sbfixunknownlength: true,
      sbfixaddcommssteamid: true,
      /* sbfixplayernotfoundnameencoding: true, */
      sbpartiallydelayloading: true,
      sbaddcustompunishmenttime: true
    },
    function(details) {
      if (details.sourcebans) addCode('sourcebans', details.sourcebans_css);
      let panel = getUrlParameter('p');
      if (details.sbfixlockup) {
        document.addEventListener('mouseout', e => e.stopImmediatePropagation(), true);
        document.addEventListener('mouseover', e => e.stopImmediatePropagation(), true);
      }
      if (details.sbfixunknownlength && panel == 'admin' && getUrlParameter('o') == 'edit') {
        let fixUnknownLength = document.createElement('script');
        fixUnknownLength.setAttribute('type', 'text/javascript');
        fixUnknownLength.appendChild(
          document.createTextNode(
            `
          var oldselectLengthTypeReason = selectLengthTypeReason;
          selectLengthTypeReason = function(length, type, reason) {
              for (var i = 0; i < $('banlength').length; i++) {
                  if ($('banlength').options[i].value == (length / 60)) {
                      oldselectLengthTypeReason(length, type, reason);
                      return
                  }
              };
              oldselectLengthTypeReason(0, type, reason);
              if ({0}) {
                $('banlength').parentElement.insertAdjacentHTML('beforeend', '<div style=\"color: #e8e8e8;font-weight: bold;display: inline\">Original: ' + sectodate(length) + '</div>');

                function sectodate(secs) {
                    let ret = '';
                    let timeS = [2592000, 604800, 86400, 3600, 60, 1];
                    let timeC = ['mo', 'wk', 'd', 'hr', 'min', 'sec'];
                    for (let index in timeS) {
                        value = Math.floor(secs / timeS[index]);
                        if (value > 0) {
                            ret += value + ' ' + timeC[index] + ', ';
                            secs %= timeS[index]
                        }
                    };
                    return ret.slice(0, -2)
                }
                return
              }
              let lastOption = $('banlength').options[$('banlength').options.length - 1];
              let txtLength = $('txtLength');
              lastOption.selected = true;
              txtLength.value = lastOption.value = length / 60;
              txtLength.setAttribute('style', 'display: block');
          }
        `.format(details.resettopermanent)
          )
        );
        let dah = new MutationObserver(() => {
          // if I check fo sourcebans.js it's too early apparently
          if (document.querySelector('script[src="./scripts/mootools.js"]')) {
            document.head.appendChild(document.importNode(fixUnknownLength, true));
            dah.disconnect();
          }
        });
        dah.observe(document, { childList: true, subtree: true });
      }
      //if (details.sbfixplayernotfoundnameencoding && panel == 'servers') {
      //  // might fix issues
      //  let fixUncodedURL = document.createElement('script');
      //  fixUncodedURL.setAttribute('type', 'text/javascript');
      //  fixUncodedURL.appendChild(
      //    document.createTextNode(
      //      `document.addEventListener('DOMContentLoaded', () =>
      //    {
      //      var oldAddContextMenu = AddContextMenu;
      //      AddContextMenu = function (select, classNames, fader, headl, oLinks)
      //      {
      //        for (var i = 0; i < oLinks.length; i++)
      //        {
      //          if (!oLinks[i].callback) continue;
      //          var strfunc = oLinks[i].callback.toString();
      //          if (strfunc.includes('window.location = "index.php?p=admin&c='))
      //          {
      //            var matches = strfunc.match(/.*"(.*pName=)(.*)".*/);
      //            (function (matches) {
      //              oLinks[i].callback = function ()
      //              {
      //                window.location = matches[1] + encodeURIComponent(matches[2]);
      //              };
      //            })(matches);
      //          }
      //        }
      //        oldAddContextMenu(select, classNames, fader, headl, oLinks);
      //      };
      //    });`
      //    )
      //  );
      //  let dah = new MutationObserver(() => {
      //    if (document.querySelector('script[src="./scripts/contextMenoo.js"]')) {
      //      document.head.appendChild(document.importNode(fixUncodedURL, true));
      //      dah.disconnect();
      //    }
      //  });
      //  dah.observe(document, { childList: true, subtree: true });
      //}
      if (panel == 'banlist' || panel == 'commslist') {
        if (details.sbpartiallydelayloading) {
          let FIXTHEDAMNLAG = new MutationObserver(() => {
            [].forEach.call(document.querySelectorAll('script[type="text/javascript"]'), el => {
              if (el.innerText.includes("xajax_ServerHostPlayers('101',")) {
                el.innerText = `
            function onClickLoadServers(){
              setTimeout(() => {
                {0}
              }, 0);
              document.querySelector('tr.sea_open').removeEventListener('click', onClickLoadServers);
            }
            document.querySelector('tr.sea_open').addEventListener('click', onClickLoadServers);
            `.format(el.innerText);
                FIXTHEDAMNLAG.disconnect();
                return;
              }
            });
          });
          FIXTHEDAMNLAG.observe(document, { childList: true, subtree: true });
        }
        document.addEventListener('DOMContentLoaded', addExtra);
      }
      if (details.sourcebans || details.sblogolinktoforums) {
        let fixLogo = new MutationObserver(() => {
          let badlogo = document.querySelector('img[alt="SourceBans Logo"]');
          if (badlogo) {
            if (details.sourcebans) badlogo.src = 'https://www.panda-community.com/styles/io_dark/io/images/logo-white.png';
            badlogo.setAttribute('style', 'height: 80px');
            if (details.sblogolinktoforums) badlogo.parentElement.setAttribute('href', 'https://panda.tf');
            fixLogo.disconnect();
          }
        });
        fixLogo.observe(document, { childList: true, subtree: true });
      }
      if (details.sbaddcustompunishmenttime && panel == 'admin' && (getUrlParameter('c') == 'comms' || getUrlParameter('c') == 'bans')) {
        let addCustomLength = new MutationObserver(() => {
          let banlength = document.querySelector('select[id="banlength"]');
          if (banlength) {
            let el = document.createElement('option');
            el.value = -1;
            el.innerText = 'Custom';
            el.setAttribute('iscustom', '1');
            banlength.appendChild(el);
            banlength.addEventListener('click', () => {});
            let divel = document.createElement('div');
            let tempel = document.createElement('textarea');
            setAttributes(tempel, {
              class: 'textbox',
              cols: '5',
              rows: '1',
              id: 'txtLength',
              style: 'display: none',
              placeholder: 'MINUTES'
            });
            function setValue(val) {
              el.value = val ? val : -1;
            }
            tempel.addEventListener('paste', function(e) {
              let pasteText = e.clipboardData.getData('text');
              let result = '';
              for (var i = 0; i < pasteText.length; i++) {
                if (isNaN(pasteText[i]) || pasteText[i] === ' ') continue;
                result += pasteText[i];
              }
              e.preventDefault();
              this.value += result;
              setValue(this.value);
            });
            tempel.addEventListener('keyup', function() {
              setValue(this.value);
            });
            tempel.addEventListener('keypress', function(e) {
              if (isNaN(e.key)) e.preventDefault();
              setValue(this.value);
            });
            divel.appendChild(tempel);
            banlength.addEventListener('change', function() {
              tempel.setAttribute('style', 'display: ' + (this[this.selectedIndex].getAttribute('iscustom') ? 'block' : 'none'));
            });
            banlength.parentElement.appendChild(divel);
            addCustomLength.disconnect();
          }
        });
        addCustomLength.observe(document, { childList: true, subtree: true });
        document.addEventListener('DOMContentLoaded', () => {
          let addCustomLegnthSupportEl = document.createElement('script');
          addCustomLegnthSupportEl.setAttribute('type', 'text/javascript');
          addCustomLegnthSupportEl.appendChild(
            document.createTextNode(`
          var oldProcessBan = ProcessBan;
          ProcessBan = function() {
            let banlength = $('banlength').value;
            if (banlength < 0 || !banlength)
              return;
            oldProcessBan();
          }
        `)
          );
          [].forEach.call(document.querySelectorAll('script[type="text/javascript"]'), el => {
            if (el.innerText.includes('function ProcessBan()')) {
              el.parentElement.appendChild(addCustomLegnthSupportEl);
            }
          });
        });
      }
    }
  );

  function addExtra() {
    [].forEach.call(document.getElementById('banlist').querySelectorAll('div.ban-edit'), el => {
      let parent = el.parentElement.parentElement.parentElement;
      let id = '';
      let trs = parent.querySelectorAll('tr');
      for (let i = 0; i < trs.length; i++) {
        let tr = trs[i];
        if (tr.querySelectorAll('td')[0].innerText.includes('Steam ID')) {
          id = tr.querySelectorAll('td')[1].innerText;
          break;
        }
      }
      if (id.includes('STEAM_')) {
        let list = el.querySelector('ul');
        list.insertAdjacentHTML(
          'beforeend',
          '<li><a class="PTB-button" href="https://bans.panda-community.com/index.php?p=banlist&searchText={0}">Search Bans</a> </li><li><a class="PTB-button" href="https://bans.panda-community.com/index.php?p=commslist&searchText={0}">Search Comms</a> </li><li><a class="PTB-button" href="https://gameme.panda-community.com/search?si=players&rc=all&q={0}">gameME</a></li><li><a id="altCheck" class="PTB-button" href="https://staff.panda-community.com/altlookup">Alt check</a></li>'
          .format(id)
          );
        (function(id) {
          list.querySelector('#altCheck').addEventListener('click', () => chrome.storage.local.set({ altsearch: id }));
          list.querySelector('#altCheck').addEventListener('auxclick', () => chrome.storage.local.set({ altsearch: id }));
          [].forEach.call(list.getElementsByClassName('PTB-button'), el => {
            function handleClickEvent(event) {
              window.open(this.getAttribute('href').format(id));
              event.preventDefault();
            }
            el.addEventListener('click', handleClickEvent);
            el.addEventListener('auxclick', handleClickEvent);
          });
        })(id);
      }
    });
  }
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(document.location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  }
})();
