(function() {
  chrome.storage.local.get({ gameme: true, gameme_css: '', gameme_border: '' }, function(details) {
    if (details.gameme) {
      addCode('gameme', details.gameme_css);
      if (details.gameme_border != 'dark') addCode('gameme_border', gameME_border_map[details.gameme_border]);
    }
    addCode('resizelogo', 'img[title="Logo"]{height: 134px;}');
    if (details.gameme) {
      let fixLogo = new MutationObserver(() => {
        let badlogo = document.querySelector('img[title="Logo"]');
        if (badlogo) {
          badlogo.src = 'https://www.panda-community.com/styles/io_dark/io/images/logo-white.png';
          fixLogo.disconnect();
        }
      });
      fixLogo.observe(document, { childList: true, subtree: true });
    }
    document.addEventListener('DOMContentLoaded', () => {
      if (document.URL.includes('/playerinfo/')) {
        let id = '';
        {
          let trs = document.querySelector('table.spacer_b tbody').querySelectorAll('tr');
          for (let i = 0; i < trs.length; i++) {
            if (trs[i].querySelectorAll('td').length < 1) continue;
            let txt = trs[i].querySelectorAll('td')[1].innerText;
            if (txt.includes('STEAM_')) {
              id = txt;
              break;
            }
          }
        }
        let list = document.querySelector('div.cont_right ul.inl');
        list.insertAdjacentHTML(
          'beforeend',
          '<li><a class="PTB-button" href="https://bans.panda-community.com/index.php?p=banlist&searchText={0}">Search Bans</a> </li><li><a class="PTB-button" href="https://bans.panda-community.com/index.php?p=commslist&searchText={0}">Search Comms</a> </li><li><a class="PTB-button" href="https://gameme.panda-community.com/search?si=players&rc=all&q={0}">gameME</a> </li><li><a id="altCheck" class="PTB-button" href="https://staff.panda-community.com/altlookup">Alt check</a></li>'
        );
        (function(id) {
          list.querySelector('#altCheck').addEventListener('click', () => chrome.storage.local.set({ altsearch: id }));
          list.querySelector('#altCheck').addEventListener('auxclick', () => chrome.storage.local.set({ altsearch: id }));
          [].forEach.call(list.getElementsByClassName('PTB-button'), el => {
            function handleClickEvent(event) {
              window.open(this.getAttribute('href').format(id));
              event.preventDefault();
            }
            el.addEventListener('click', handleClickEvent);
            el.addEventListener('auxclick', handleClickEvent);
          });
        })(id);
      }
    });
  });

  const gameME_border_map = {
    none: 'td,th{border:0px solid #000}',
    light: 'td,th{border:1px solid #585858}'
  };
})();
