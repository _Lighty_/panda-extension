String.prototype.format = function () {
  var content = this;
  for (var i = 0; i < arguments.length; i++) {
    var replacement = '{' + i + '}';
    content = content.replace(replacement, arguments[i]);
  }
  return content;
};
/*
PRIVATE CHANGELIST

-Subforum ignoring (WIP only new/recent threads as of right now) (not done yet) (TODO)

*/
var updateMSG =
  `
  -Removed unneeded sourcebans fixes<br>
  -All sourcebans fixes are and features are toggleable now<br>
  -Fixed sourcebans CSS issues and minor errors<br>
  -Added a no bdays condition (widget is smaller, some text is displayed)<br>
  -new/recent threads tabs now properly transition and animate
  `;

var gameme_css =
  'body{background-color:#121212}div.header{border:0 solid #495425;background-image:url(https://s1.gameme.net);background-color:#202020}div.nav_bar{background-color:#545454}div.nav_bar,div.nav_bar a{color:#dadada}th{color:#a9a9a9;background-color:#333}tr.ranking_line td,tr.server_line td{background-color:#212121;color:#cacaca}.srv_empty{color:#585858}.footer p,a.ext:link,a.ext:visited,a.rnk:link,a.rnk:visited,a.srv:link,a.srv:visited,a:link,a:visited,div h3,i,p.right,p.summary,th a:link,th a:visited,tr.ranking_line a,tr.server_line a,tr.server_line td,ul.sections li a:link,ul.sections li a:visited{color:#afafaf}div.container table{border-left:0 solid #000;border-right:0 solid #000;border-bottom:0 solid #000}.t_sc,input,td{color:#afafaf}input{background-color:#212121;border-width:0;font-size:12px}.t_sc,.weapon,.wpn_bg,td{background-color:#333}textarea{background-color:#464646!important;color:#c1c1c1!important;border-width:0}tr.team_a_fc td{color:#0af;background-color:#074f7f}tr.team_a_summary_fc td{border-top:0 solid #0080c0;color:#0af;background-color:#002d4c}tr.team_b_fc td{color:red;background-color:#4c0000}tr.team_b_summary_fc td{border-top:0 solid #ff2d2d;color:red;background-color:#2f0000}table.calendar td.selected{background-color:#636363}';
var sourcebans_css =
  '.front-module-intro{display:none}body,div[style="background-color: #bab5b2;"]{background-color:#121212!important}#header{height:125px}#admin-page-menu>a{background-color:#2b2b2b}#search{margin-top:21px}#head-logo{margin-top:30px}#tabsWrapper{background-color:#252525}#front-servers{margin-bottom:10px}#admin-page-menu ul li a,#footer{background-color:#252525}#tabs ul li.active a{background-color:#1b1b1b}#admin-page-menu ul li a:hover,#admin-page-menu ul li.active a:hover,#tabs ul li a:hover,#admin-page-menu>a:hover{background-color:#4a4a4a}h3{background-color:#272727}td.listtable_1,td.listtable_top{background-color:#333;border-right:0 solid #cfcac6!important;border-left:0 solid #cfcac6!important}td.listtable_1{background-color:#212121;color:#9e9e9e}td.listtable_1_banned{color:#000}td.listtable_2{border-width:0;background-color:#2b2b2b}.ban-edit li a{border-bottom:0 solid #ddd}.select,.textbox,input.searchbox,input.loginmedium{background:#3c3c3c;border:0 solid #ccc;color:#9e9e9e}select.submit-fields,input.submit-fields,textarea.submit-fields, div > select{background:#3c3c3c;border:2px solid #3c3c3c;color:#9e9e9e}.ok{background-color:#5885a2;border-color:#5885a2}div.contextmenu,hr{border:0 solid #ddd}#banlist-nav,#breadcrumb,#content_title,#nav a,a:link,a:visited{color:#a5a5a5}#cpanel{background-color:#212121}#admin-page-content{background-color:#2b2b2b}.rowdesc{color:#e8e8e8}p{color:#c3c3c3}div.contextmenu{background-color:#383838;border-left:0 solid #ddd}.contextmenu .head,.contextmenu a:hover{background-color:#6d6d6d}.contextmenu .separator{border-bottom:1px solid #616161;background:#0c0c0c00}.contextmenu a{margin:0}.dialog-content,div.dialog-control{background:#292929 0%}div.dialog-body{border-bottom:0 solid #ccc;color:#a5a5a5}.dialog-content{border-width:0}img[src="images/logos/sb-panda.png"]{';
var staffpage_css =
  'body,body.pushable>.pusher{background-color:#121212!important}.ui.header{color:#b7b7b7}.ui.card,.ui.cards>.card{background-color:#202020;box-shadow:0 0 0 0 #d4d4d5,0 0 0 0 #d4d4d5}.ui.card .meta,.ui.cards>.card .meta{color:#c3c3c3}.ui.card>.content>.header,.ui.cards>.card>.content>.header{color:rgba(212,212,212,.85)}.ui.card>.extra a:not(.ui),.ui.cards>.card>.extra a:not(.ui){color:#c1c1c1}.ui.card>.extra,.ui.cards>.card>.extra{color:#c1c1c1}.ui.form input[type=text]{background:#464646;border:0 solid rgba(34,36,38,.15);color:#d0d0d0}.ui.form input[type=text]:focus{background:#2f2f2f;border-color:transparent;color:#c1c1c1}.ui.action.input:not([class*="left action"]) input:focus{border-right-color:#000!important}.ui.styled.accordion{background:#202020;box-shadow:0 1px 2px 0 rgba(34,36,38,.15),0 0 0 1px rgba(34,36,38,.15)}.ui.styled.accordion .title{color:#bfbfbf}.ui.styled.accordion .active.title{color:rgba(202,202,202,.95)}.ui.accordion .active.content{color:#b9b9b9}.ui.menu .dropdown.item .menu{background:#3d3e3f;box-shadow:0 0 0 0 rgba(0,0,0,.08);margin:0;min-width:calc(100% - 0px)}.ui.dropdown .menu{border:0 solid rgba(34,36,38,.15)}.ui.menu .ui.dropdown .menu>.item{color:#b9b9b9!important}.ui.menu{background:#131313;box-shadow:0 0 0 0 rgba(34,36,38,.15)}.ui.item.menu{background:#202020}.ui-datepicker .ui-datepicker-calendar th,.ui-datepicker a,.ui.checkbox label,.ui.form .field>label,.ui.menu .item,.ui.pagination.menu .active.item{color:#c1c1c1}.ui.raised.segment{box-shadow:0 0 0 0 rgba(34,36,38,.12),0 0 0 0 rgba(34,36,38,.15)}.ui.list>a.item:hover .icon,.ui.menu .item.disabled,.ui.menu .item.disabled:hover,.ui.menu a.item:hover{color:#737373}.ui.form textarea,.ui.menu,.ui.segment{border:0 solid rgba(34,36,38,.15)}.ui.segment{background:#202020}.ui-datepicker{background-color:#333;border-width:0}.ui.form textarea{background:#464646;color:rgba(191,191,191,.87)}.ui.form textarea:focus{background:#2d2d2d;border-color:#000;color:#c1c1c1}.ui.checkbox label:hover{color:rgba(156,156,156,.8)}.ui.comments .comment .author,.ui.comments .comment .metadata,.ui.comments .comment .text,.ui.form .inline.field>label,.ui.list>a.item i.icon,.ui.table,.ui.table thead th,b{color:#c1c1c1}.ui.horizontal.list:not(.celled)>.item:first-child{color:#c1c1c1}.ui.form .field:last-child,p:last-child{color:#c1c1c1}.ui.selection.dropdown{background:#464646;color:#c1c1c1}.ui.selection.active.dropdown,.ui.selection.active.dropdown:hover{border-color:#96c8da00;box-shadow:0 0 0 0 rgba(34,36,38,.15)}.ui.dropdown .menu .selected.item,.ui.dropdown .menu>.item,.ui.message{color:#c1c1c1}.ui.selection.dropdown .menu>.item{border-top:0 solid #fafafa}.ui.dropdown .menu .selected.item{background:rgba(0,0,0,.19)}.ui.dropdown .menu{background:#464646}.ui.dropdown .menu>.item:hover{background:#656565}.ui.message{background:#202020}.ui.selection.visible.dropdown>.text:not(.default){color:#c1c1c1}.ui.menu .active.item{background:#3a3a3a;color:#c1c1c1}.ui.label,.ui.menu .active.item:hover,.ui.styled.accordion .content{color:#c1c1c1}.ui.negative.message{box-shadow:0 0 0 0 #e0b4b4 inset,0 0 0 0 transparent;background-color:#202020;color:#9f3a38}.ui.menu .ui.dropdown .menu>.active.item{color:#c1c1c1!important}.ui.label{background-color:#505050}.ui.styled.accordion .title:hover{color:#868686}';
let geturl = chrome.runtime.getURL;
var newemotes_css = 'img[alt=":)"]{background:url({0})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":3:"]{background:url({1})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":nekoheart:"]{background:url({2})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":x3:"]{background:url({3})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=";)"]{background:url({4})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":("]{background:url({5})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":mad:"]{background:url({6})!important;height: 18px!important;padding-left: 18px!important;width: 0!important;background-size: 18px 18px !important}img[alt=":confused:"]{background:url({7})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":cool:"]{background:url({8})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":p"]{background:url({9})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":D"]{background:url({10})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":eek:"]{background:url({11})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":oops:"]{background:url({12})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt=":rolleyes:"]{background:url({13})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}img[alt="o_O"]{background:url({14})!important;height: 18px!important;padding-left: 18px!important;width: 0!important}'.format(
  geturl('data/emotes/happy.svg'),
  geturl('data/emotes/cat_smile.svg'),
  geturl('data/emotes/heart.svg'),
  geturl('data/emotes/cat_smile_closed_eyes.svg'),
  geturl('data/emotes/wink.svg'),
  geturl('data/emotes/sad.svg'),
  geturl('data/emotes/mad.svg'),
  geturl('data/emotes/confused.svg'),
  geturl('data/emotes/cool.svg'),
  geturl('data/emotes/stick_out_tongue.svg'),
  geturl('data/emotes/smile.svg'),
  geturl('data/emotes/surprised.svg'),
  geturl('data/emotes/oops.svg'),
  geturl('data/emotes/rolls_eyes.svg'),
  geturl('data/emotes/O_o.svg')
);
var options = {
  gameme: true,
  gameme_border: 'none',
  sourcebans: true,
  staffpage: true,
  extrashoutboxbuttons: true,
  contextmenus: true,
  staffpage_autologin: true,
  staffpage_group_alts: true,
  disablesnow: true,
  fixshoutbox: true,
  removeonlinestaff: false,
  version: '0',
  tabbedthreadview: true,
  sblogolinktoforums: false,
  shoutboximagesize: '0',/*
  ignorethreads: true,
  ignorenodes: true,
  ignoredthreadids: {},
  ignorednodeids: {},*/
  fixsidebarbeinglaggy: true,
  shoutboxtimeonright: true,
  resettopermanent: true,
  birthdayswidget: true,
  staffpagecloseredirecttab: false,
  sbfixlockup: true,
  sbfixunknownlength: true,
  /* sbfixplayernotfoundnameencoding: true, */
  sbpartiallydelayloading: true,
  sbaddcustompunishmenttime: true,
  staffpagecallbotmode: false,
};
chrome.storage.local.remove(['gameme_css', 'staffpage_css', 'sourcebans_css', 'newemotes_css']);
chrome.storage.sync.get(options, items => {
  chrome.storage.local.set({
    newemotes_css: newemotes_css,
    gameme_css: gameme_css,
    sourcebans_css: sourcebans_css,
    staffpage_css: staffpage_css,
    updateMSG: updateMSG.replace(/[']/g, '\\\'').replace(/[\"]/g, '\\"').replace(/\n/g, '') // shut up
  });
  contexts(items.contextmenus);
  setCloseRedirectTab(items.staffpagecloseredirecttab);
  var ver = chrome.runtime.getManifest().version;
  var needsNotify = items.version !== ver && parseInt(items.version.replace(/\./g, '')) < parseInt(ver.replace(/\./g, ''));
  chrome.storage.local.set(items);
  if (needsNotify) chrome.tabs.query({ url: '*://www.panda-community.com/*' }, tabs => tabs.forEach(t => chrome.tabs.reload(t.id)));
});
setTimeout(updateVars, 300000);
function updateVars() {
  chrome.storage.local.clear(() => {
    chrome.storage.sync.get(options, i => {
      if (i.shoutboximagesize > 100) {
        let newImageSize = parseInt(parseInt(i.shoutboximagesize) / 2000) * 100;
        chrome.storage.sync.set({
          shoutboximagesize: newImageSize.toString()
        });
        i.shoutboximagesize = newImageSize;
      }
      chrome.storage.local.set({
        newemotes_css: newemotes_css,
        gameme_css: gameme_css,
        sourcebans_css: sourcebans_css,
        staffpage_css: staffpage_css
      });
      contexts(i.contextmenus);
      setCloseRedirectTab(i.staffpagecloseredirecttab);
      chrome.storage.local.set(i);
    });
    setTimeout(updateVars, 300000);
  });
}
var activetab = '';
chrome.storage.local.get(['activetab'], i => {
  activetab = i.activetab;
});
/*
function setblockJS(val) {
  if (val) {
    if (!chrome.webRequest.onBeforeRequest.hasListener(blockRequest))
      chrome.webRequest.onBeforeRequest.addListener(
          blockRequest,
          {urls: ['*://www.panda-community.com/js/dark/taigachat.js*']},
          ['blocking']);
  } else if (chrome.webRequest.onBeforeRequest.hasListener(blockRequest))
    chrome.webRequest.onBeforeRequest.removeListener(blockRequest);
}

function blockRequest(details) {
  return {cancel: true};
}
*/
/* DON'T TOUCH THIS */
/*function logError(responseDetails) {
  // Do nothing much useful listener
}

chrome.webRequest.onErrorOccurred.addListener(logError, {urls:
['<all_urls>']});*/
/* DON'T TOUCH THIS */

function handleReq(req) {
  setTimeout(() => chrome.tabs.remove(req.tabId), 200);
}

function setCloseRedirectTab(val) {
  if (val) {
    if (!chrome.webRequest.onBeforeRedirect.hasListener(handleReq))
      chrome.webRequest.onBeforeRedirect.addListener(handleReq, {
        urls: ['https://staff.panda-community.com/join/*']
      });
  } else if (chrome.webRequest.onBeforeRedirect.hasListener(handleReq)) {
    chrome.webRequest.onBeforeRedirect.removeListener(handleReq);
  }
}

function createShortURLAndSaveToClipboard(url) {
  let matches = url.match(/.*:\/\/((?!www).*\.)?.*?\.com\/(.*)/);
  let subdomain = matches[1];
  if (!subdomain) subdomain = '';
  let shortened = 'https://{0}panda.tf/{1}'.format(subdomain, matches[2]);
  let shorterdata = shortened.match(/(.*\/)(?:[^\/]+\.([\d]+)).*$/);
  if (shorterdata) shortened = shorterdata[1] + shorterdata[2];
  document.oncopy = function (event) {
    event.clipboardData.setData('text/plain', shortened);
    event.preventDefault();
  };
  document.execCommand('copy', false, null);
}

function contexts(val) {
  chrome.contextMenus.removeAll();
  if (val) {
    chrome.contextMenus.create({
      title: 'Search bans',
      contexts: ['selection'],
      onclick: text => openURL('https://bans.panda-community.com/index.php?p=banlist&searchText=' + text.selectionText)
    });

    chrome.contextMenus.create({
      title: 'Search comms',
      contexts: ['selection'],
      onclick: text => openURL('https://bans.panda-community.com/index.php?p=commslist&searchText=' + text.selectionText)
    });

    chrome.contextMenus.create({
      title: 'Search gameME',
      contexts: ['selection'],
      onclick: text => openURL('https://gameme.panda-community.com/search?si=players&rc=all&q=' + text.selectionText)
    });

    chrome.contextMenus.create({
      title: 'Search alts',
      contexts: ['selection'],
      onclick: text => searchAlt(text.selectionText)
    });
    chrome.contextMenus.create({
      title: 'Copy shortened link',
      contexts: ['page'],
      documentUrlPatterns: ['*://*.panda-community.com/*'],
      onclick: data => createShortURLAndSaveToClipboard(data.pageUrl)
    });
  }
}

function searchAlt(id) {
  chrome.storage.local.set({ altsearch: id });
  openURL('https://staff.panda-community.com/altlookup');
}

function openURL(url) {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    var currTab = tabs[0];
    if (currTab) {
      chrome.tabs.create({ url: url, index: currTab.index + 1 });
    }
  });
}
